const menuToggle = document.querySelector("#menu-toggle");
const imgOne = document.querySelector("#menu-one");
const imgTwo = document.querySelector("#menu-two");
const menuList = document.querySelector("#menu-list");
const language = document.querySelector("#langimg");
const languageBlock = document.querySelector("#language_block");
const searchBox = document.querySelector(".search-box");
const searchBtn = document.querySelector(".search-btn");
const search = document.querySelector(".search");

searchBtn.addEventListener("click", function () {
  if (search.classList.contains("active")) {
    // searchBox.value = "";
  } else {
    search.classList.add("active");
    searchBox.focus();
  }
});

menuToggle.addEventListener("click", function () {
  if (imgOne.style.display == "none") {
    imgOne.style.display = "block";
    imgTwo.style.display = "none";
    menuList.style.display = "none";
  } else {
    imgOne.style.display = "none";
    imgTwo.style.display = "block";
    menuList.style.display = "block";
  }
});
language.addEventListener("click", function (e) {
  if (languageBlock.style.display == "block") {
    languageBlock.style.display = "none";
  } else {
    languageBlock.style.display = "block";
  }
});
document.body.addEventListener("click", function (e) {
  console.log(e.target.id);
  if (e.target?.id != "menu-one" && e.target?.id != "menu-two") {
    if (imgOne.style.display == "none") {
      imgOne.style.display = "block";
      imgTwo.style.display = "none";
      menuList.style.display = "none";
    }
  }
  if (e.target?.id != "langimg") {
    if (languageBlock.style.display == "block") {
      languageBlock.style.display = "none";
    }
  }

  if (e.target?.id != "search-input" && e.target?.id != "search-box") {
    search.classList.remove("active");
    searchBox.value = "";
  }
});
